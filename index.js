class DataManager {
  constructor(api_endpoint) {
    this.api_endpoint = api_endpoint;
    this.credentials = "prod_user:unique_snowflake";
  }

  /**
   * Fetches the path /a_path of API endpoint
   */
  fetch_a() {
    const url = `${api_endpoint}/a_path.txt`;
    const headers = {
      Authorization: `Basic ${Buffer.from(this.credentials).toString(
        "base64"
      )}`,
    };
    console.log(
      `Sending a request to ${this.api_endpoint} with credentials ${this.credentials}`
    );
    fetch(url, { headers })
      .then((res) => {
        const data = JSON.parse(res.text);
        return data;
      })
      .catch((err) => {
        console.log(`Failed with err: ${err}`);
      });
  }

  fetch_b() {
    const url = `${api_endpoint}/b_path.json`;

    fetch(url)
      .then((res) => {
        const data = JSON.parse(res.text);
        return data;
      })
      .catch((err) => {
        console.log(`Failed with err: ${err}`);
      });
  }
}
